//A18 prefablist, November 10

//csv format:
//prefab_filename, 7DtD zone (ignored), face north, y-offset, x,y,z, NitroGen zone (; as delimiter)

//placement zone types:
//alone  -> single POI in the countryside, camps, bases, huts
//mountain -> POIs spawned above a certain hight, like cabins
//farm	-> farmhouses and fileds, watertower
//houses -> family houses
//smalltown ->buildings apart from houses that are fitting to represent smaller towns
//downtown -> shops, bars
//industrial ->industrial buildings
//citycenter -> skycrapers, detroyed houses, large government buildings
//trader -> traders are taken from this pool of POIs
//oldwest -> oldwest buildings
//hillbillytrailer, hillbillyjunk -> trailer parks with scrap yards (just a silly stereotype)
//carlot -> indicates POIs with cars in them (more spawned for balancing)
//unique  -> special marker to spawn POI only once at most (assure that there is at least one non unique POI of each zone type!!)

//several POIs are uncommented, as they are either very specific navezgane handplaced POIs or would require more complex pacement logic in specific locations


//custom zone type (for modding)
//add POIs to those custom zones. The settlements will only spawn if any POI is assigned to them, else the custom settlement is ignored

//(towns have a main aphalt road, the number indicates the number of POIs spawned in this custom town)
//custom_town_size_25
//custom_town_size_18
//custom_town_size_15
//custom_town_size_12
//custom_town_size_10
//custom_town_size_8

//(settlements have a gravel road)
//custom_settlement_size_10
//custom_settlement_size_8
//custom_settlement_size_5
//custom_settlement_size_3

//(outback POIs have a single dead-end road leading towards them)
//custom_outback_size_4
//custom_outback_size_3
//custom_outback_size_2
//custom_outback_size_1


abandoned_house_01,RESIDENTIALOLD,2,-1,24,21,26,houses
abandoned_house_02,RESIDENTIALOLD,2,-1,24,17,25,houses
abandoned_house_03,RESIDENTIALOLD,2,-1,21,16,28,houses
abandoned_house_04,RESIDENTIALOLD,2,-1,29,16,30,houses
abandoned_house_05,RESIDENTIALOLD,2,-5,26,22,30,houses
abandoned_house_06,RESIDENTIALOLD,2,-1,30,17,31,houses
abandoned_house_07,RESIDENTIALOLD,2,-1,34,18,29,houses
abandoned_house_08,RESIDENTIALOLD,2,-10,28,29,34,houses
apartment_adobe_red_5_flr,RESIDENTIALNEW;DOWNTOWN,2,0,45,36,37,citycenter;downtown
apartment_brick_6_flr,RESIDENTIALNEW;DOWNTOWN,2,-7,55,61,57,citycenter;downtown
army_barracks_01,INDUSTRIAL,2,-5,51,40,97,alone
army_camp_01,INDUSTRIAL,2,-3,53,22,57,alone;mountain
army_camp_02,INDUSTRIAL,2,-2,53,12,57,alone;mountain
army_camp_03,INDUSTRIAL,2,-6,53,23,57,alone;mountain
barn_01,INDUSTRIAL,2,-1,21,11,22,farm
barn_02,INDUSTRIAL,2,-1,36,15,48,farm
barn_03,INDUSTRIAL,2,-3,50,27,56,farm
bar_sm_01,COMMERCIAL;DOWNTOWN,2,-1,24,10,37,downtown
bar_stripclub_01,COMMERCIAL;DOWNTOWN,2,-1,33,17,51,downtown
bar_theater_01,COMMERCIAL;DOWNTOWN,2,-1,40,21,43,downtown
blueberryfield_sm,RESIDENTIALOLD;INDUSTRIAL,2,-1,12,2,12,farm
bluff01,BIOMEONLY,0,-4,15,10,19,alone
bombshelter_lg_01,RESIDENTIALOLD,2,-10,27,22,37,houses
bombshelter_md_01,RESIDENTIALOLD,2,-12,29,22,34,houses
//TODO for roads:
//bridge_10x10_end_01,BRIDGE,2,-1,10,18,10,
//bridge_10x10_tile_01,BRIDGE,2,-1,10,18,10,
//bridge_asphalt1,BRIDGE,2,0,39,11,11,
//bridge_asphalt_broken,BRIDGE,2,0,39,16,11,
//bridge_concrete1,BRIDGE,2,0,39,11,11,
//bridge_wood1,BRIDGE,2,0,39,11,9,
//bridge_wood2,BRIDGE,2,0,32,10,7,
business_burnt_01,INDUSTRIAL,2,-1,28,25,35,downtown
business_burnt_02,INDUSTRIAL,2,-1,27,26,34,downtown
business_old_01,COMMERCIAL;DOWNTOWN,2,-1,19,18,47,downtown
business_old_02,COMMERCIAL;DOWNTOWN,2,-1,23,28,48,downtown
business_old_03,COMMERCIAL;DOWNTOWN,2,-3,28,21,50,downtown
business_old_04,COMMERCIAL;DOWNTOWN,2,-1,29,25,46,downtown
business_old_05,COMMERCIAL;DOWNTOWN,2,-1,25,23,39,downtown
business_old_06,COMMERCIAL;DOWNTOWN,2,-1,28,19,65,downtown
business_old_07,COMMERCIAL;DOWNTOWN,2,-1,23,19,53,downtown
business_old_08,COMMERCIAL;DOWNTOWN,2,-1,27,26,42,downtown
business_strip_old_01,COMMERCIAL;DOWNTOWN,2,-1,44,31,43,downtown
business_strip_old_02,COMMERCIAL;DOWNTOWN,2,-1,51,23,43,downtown
business_strip_old_03,COMMERCIAL;DOWNTOWN,2,-5,56,32,32,downtown
business_strip_old_04,COMMERCIAL;DOWNTOWN,2,-6,39,32,32,downtown
bus_stop_01,NONE,0,-1,29,5,9,smalltown;downtown
bus_wreck_01,NONE,0,-4,27,7,7,smalltown;downtown;hillbillyjunk
cabin_01,RESIDENTIALOLD,2,-1,26,15,33,alone;mountain
cabin_02,RESIDENTIALOLD,2,-4,24,17,37,alone;mountain
cabin_03,RESIDENTIALOLD,2,-1,33,17,37,alone;mountain
cabin_04,RESIDENTIALOLD,2,-1,29,16,33,alone;mountain
cabin_05,RESIDENTIALOLD,2,-4,37,21,41,alone;mountain
cabin_06,RESIDENTIALOLD,2,-6,39,20,32,alone;mountain
cabin_07,RESIDENTIALOLD,2,-1,42,19,48,alone;mountain
cabin_08,RESIDENTIALOLD,2,-1,38,20,38,alone;mountain
cabin_09,RESIDENTIALOLD,2,-1,28,19,34,alone;mountain
cabin_10,RESIDENTIALOLD,2,-6,27,13,52,alone;mountain
cabin_11,RESIDENTIALOLD,2,-12,38,26,42,alone;mountain
cabin_12,RESIDENTIALOLD,0,-3,30,25,30,alone;mountain
cabin_13,RESIDENTIALOLD,2,-6,28,16,39,alone;mountain
cabin_14,RESIDENTIALOLD,0,-3,40,20,40,alone;mountain
cabin_15,RESIDENTIALOLD,2,-3,40,19,48,alone;mountain
//giftshop -> problematic placement
//canyon_gift_shop,NAVONLY,2,0,8,6,10,alone
canyon_car_wreck,NAVONLY,0,0,8,6,10,hillbillyjunk
carlot_01,COMMERCIAL,2,-1,46,16,43,industrial;downtown;smalltown;carlot
carlot_02,COMMERCIAL,2,-5,45,21,53,industrial;downtown;smalltown;carlot
carlot_03_temp,NONE,2,-1,42,21,44,industrial;downtown;smalltown;carlot
cave_01,NOZONE,2,-16,33,22,54,alone
cave_02,NOZONE,2,-16,33,23,54,alone
cave_03,NOZONE,2,-32,44,62,54,alone
cave_04,NOZONE,2,-11,33,14,54,alone
//cave_05,NONE,0,-1,26,19,61,alone
cemetery_01,RESIDENTIALOLD;RESIDENTIALNEW;DOWNTOWN,2,-2,40,17,46,smalltown
cemetery_02,RESIDENTIALOLD;RESIDENTIALNEW;DOWNTOWN,2,-12,40,32,46,smalltown
church_01,RESIDENTIALNEW;RESDENTIALOLD;DOWNTOWN,2,-23,44,69,46,smalltown
church_graveyard1,RESIDENTIALOLD,2,-2,54,30,60,smalltown
church_sm_01,RESIDENTIALOLD,2,-1,26,29,54,smalltown
cornfield_lg,RESIDENTIALOLD;INDUSTRIAL,2,-1,48,2,48,farm
cornfield_med,RESIDENTIALOLD;INDUSTRIAL,2,-1,24,2,24,farm
cornfield_sm,RESIDENTIALOLD;INDUSTRIAL,2,-1,12,2,12,farm
courthouse_med_01,DOWNTOWN,2,-10,54,61,46,citycenter
courthouse_med_02,DOWNTOWN,2,-1,62,45,35,citycenter
//courthouse_med_0T,DOWNTOWN,2,-1,64,48,35,citycenter
//departure_bridge_01,NONE,2,0,
//departure_bridge_02,NONE,2,0,
//departure_city_blk_01,FLATLOT,2,0,103,12,103
//departure_city_sign,NAVONLY,2,0,
//desert_arch,BIOMEONLY,2,-1,
//desert_form01,BIOMEONLY,2,-3,
//desert_form02,BIOMEONLY,2,0,
//diersville_city_blk_01,FLATLOT,2,0,135,12,115,
//diersville_city_blk_02,FLATLOT,2,0,135,12,115,
diner_01,COMMERCIAL;DOWNTOWN,2,-1,30,13,35,smalltown;downtown
diner_02,COMMERCIAL;DOWNTOWN,2,-1,37,18,40,smalltown;downtown
diner_03,COMMERCIAL;DOWNTOWN,2,-1,40,23,41,smalltown;downtown

//docks would require special logic to place them in and at lakes...
//docks_01,WATER,2,-11,27,22,27,alone
//docks_02,WATER,2,-9,44,37,58,alone
//docks_03,WATER,2,0,53,32,54,alone
//docks_04,WATER,2,0,12,27,25,alone
//docks_05,WATER,2,-8,30,27,25,alone
factory_lg_01,INDUSTRIAL,2,-1,92,56,98,industrial
factory_lg_02,INDUSTRIAL,2,-6,96,77,96,industrial
fastfood_01,COMMERCIAL;DOWNTOWN,2,-1,38,17,43,smalltown;downtown
fastfood_02,COMMERCIAL;DOWNTOWN,2,-1,45,24,56,smalltown;downtown
fastfood_03,COMMERCIAL;DOWNTOWN,2,-1,53,27,68,smalltown;downtown
fastfood_04,COMMERCIAL;DOWNTOWN,2,-1,33,17,51,smalltown;downtown
fire_station_01,DOWNTOWN;RESIDENTIALNEW,2,-6,32,32,43,smalltown;downtown
fire_station_02,DOWNTOWN;RESIDENTIALNEW,2,-1,38,20,44,smalltown;downtown
football_stadium,RESIDENTIALNEW,2,-1,173,44,138,smalltown
funeral_home_01,COMMERCIAL;RESIDENTIALNEW,2,-1,35,27,41,smalltown;downtown
garage_01,RESIDENTIALOLD,2,-1,13,9,37,downtown
garage_02,RESIDENTIALOLD,2,-1,14,9,37,downtown
garage_03,RESIDENTIALOLD,2,-1,15,10,37,downtown
garage_04,RESIDENTIALOLD,2,-1,28,10,39,downtown
garage_05,RESIDENTIALNEW,2,-1,26,11,40,downtown
garage_06,RESIDENTIALNEW,2,-1,29,13,38,downtown
garage_07,RESIDENTIALNEW,2,-1,19,11,37,downtown
//garage_09,NONE,2,0,111,22,43,downtown
//garage_10,NONE,2,-1,47,10,37,downtown
gas_station1,COMMERCIAL,2,-5,38,20,43,smalltown;downtown
gas_station2,COMMERCIAL,2,-1,34,13,45,smalltown;downtown
gas_station3,COMMERCIAL,2,-5,34,17,41,smalltown;downtown;carlot
gas_station4,COMMERCIAL,2,-10,33,24,44,smalltown;downtown
gas_station5,COMMERCIAL,2,-5,55,22,69,smalltown;downtown;carlot
gas_station6,COMMERCIAL,2,-2,34,15,46,smalltown;downtown
gas_station7,COMMERCIAL,2,-1,34,15,33,smalltown;downtown
gas_station8,COMMERCIAL,2,-6,29,21,48,smalltown;downtown
gas_station9,COMMERCIAL,2,-1,59,13,82,smalltown;downtown;carlot
//gravestowne_city_blk_01,NAVONLY,2,0,
//gravestowne_city_blk_half,NAVONLY,2,0,
//gravestowne_city_blk_quarter,NAVONLY,2,0,
//gravestowne_city_sign_01,NAVONLY,2,0,
hospital_01,RESIDENTIALNEW;DOWNTOWN,2,-7,127,62,111,citycenter
hotel_new_01,COMMERCIAL;DOWNTOWN,2,-5,54,21,56,citycenter
hotel_ostrich,COMMERCIAL;DOWNTOWN,2,-1,45,48,45,citycenter
hotel_roadside_01,COMMERCIAL;DOWNTOWN,2,-1,45,13,45,smalltown
hotel_roadside_02,COMMERCIAL;DOWNTOWN,2,-5,45,19,45,smalltown
house_burnt_01,RESIDENTIALOLD,2,-1,26,22,42,houses
house_burnt_02,RESIDENTIALOLD,2,-1,31,22,41,houses
house_burnt_03,RESIDENTIALOLD,2,-1,34,16,33,houses
house_burnt_04,RESIDENTIALOLD,2,-6,43,25,28,houses
house_burnt_05,RESIDENTIALOLD,2,-1,26,20,41,houses
house_burnt_06,RESIDENTIALOLD,2,-1,28,25,36,houses
house_construction_01,RESIDENTIALNEW,2,-5,32,21,46,houses
house_construction_02,RESIDENTIALNEW,2,-6,28,17,53,houses
house_modern_01,RESIDENTIALNEW,2,-1,37,18,53,houses
house_modern_02,RESIDENTIALNEW,2,-1,37,16,70,houses
house_modern_03,RESIDENTIALNEW,2,-7,47,19,55,houses
house_modern_04,RESIDENTIALNEW,2,-6,36,20,56,houses
house_modern_05,RESIDENTIALNEW,2,-1,45,20,72,houses
house_modern_06,RESIDENTIALNEW,2,-1,35,12,56,houses
house_modern_07,RESIDENTIALNEW,2,-1,37,16,52,houses
house_modern_08,RESIDENTIALNEW,2,-1,38,16,50,houses
house_modern_09,RESIDENTIALNEW,2,-1,42,16,45,houses
house_modern_10,RESIDENTIALNEW,2,-5,36,20,46,houses
house_modern_11,RESIDENTIALNEW,2,-5,37,21,43,houses
house_old_bungalow_01,RESIDENTIALOLD,2,-5,27,25,46,houses
house_old_bungalow_02,RESIDENTIALOLD,2,-4,28,25,46,houses
house_old_bungalow_03,RESIDENTIALNEW,2,-1,32,21,53,houses
house_old_bungalow_04,RESIDENTIALOLD,2,-4,29,23,46,houses
house_old_bungalow_05,RESIDENTIALOLD,2,-4,27,18,46,houses
house_old_bungalow_06,RESIDENTIALOLD,2,-7,28,23,46,houses
house_old_bungalow_07,RESIDENTIALOLD,2,-4,28,22,52,houses
house_old_bungalow_08,RESIDENTIALOLD,2,-4,28,21,53,houses
house_old_bungalow_09,RESIDENTIALOLD,2,-5,28,20,46,houses
house_old_bungalow_10,RESIDENTIALOLD,2,-2,25,14,46,houses
house_old_bungalow_11,RESIDENTIALOLD,2,-20,36,43,46,houses
house_old_bungalow_12,RESIDENTIALOLD,2,-4,23,21,46,houses
house_old_gambrel_01,RESIDENTIALOLD,2,-10,33,27,52,houses
house_old_gambrel_02,RESIDENTIALOLD,2,-1,34,21,46,houses
house_old_gambrel_03,RESIDENTIALOLD,2,-22,49,44,84,houses
house_old_mansard_01,RESIDENTIALOLD,2,-5,42,26,52,houses
house_old_mansard_02,RESIDENTIALOLD,2,-7,42,30,49,houses
house_old_mansard_03,RESIDENTIALOLD,2,-4,32,20,53,houses
house_old_mansard_04,RESIDENTIALOLD,2,-5,28,21,46,houses
house_old_mansard_05,RESIDENTIALOLD,2,-1,30,20,46,houses
house_old_mansard_06,RESIDENTIALOLD,3,-7,44,33,98,houses
//hillbillytrailer
house_old_modular_01,RESIDENTIALOLD,2,-1,31,13,41,houses;hillbillytrailer
house_old_modular_02,RESIDENTIALOLD,2,-1,31,19,41,houses;hillbillytrailer
house_old_modular_03,RESIDENTIALOLD,2,-6,31,16,41,houses;hillbillytrailer
house_old_modular_04,RESIDENTIALOLD,2,-1,31,10,41,houses;hillbillytrailer
house_old_modular_05,RESIDENTIALOLD,2,-1,31,11,41,houses
house_old_modular_06,RESIDENTIALOLD,2,-5,31,14,41,houses
house_old_modular_07,RESIDENTIALOLD,2,-1,31,12,41,houses
house_old_modular_08,RESIDENTIALOLD,2,-1,31,13,41,houses
house_old_pyramid_01,RESIDENTIALOLD,2,-5,32,25,52,houses
house_old_pyramid_02,RESIDENTIALOLD,2,-4,31,22,46,houses
house_old_pyramid_03,RESIDENTIALOLD,2,-3,28,26,46,houses
house_old_pyramid_04,RESIDENTIALOLD,2,-5,40,27,52,houses
house_old_pyramid_05,RESIDENTIALOLD,2,-4,32,18,46,houses
house_old_ranch_01,RESIDENTIALOLD,2,-1,44,23,44,houses
house_old_ranch_02,RESIDENTIALNEW,2,-3,65,21,59,houses
house_old_ranch_03,RESIDENTIALNEW,2,-7,38,22,42,houses
house_old_ranch_04,RESIDENTIALNEW,2,-7,44,23,45,houses
house_old_ranch_05,RESIDENTIALOLD,2,-4,32,23,46,houses
house_old_ranch_06,RESIDENTIALOLD,2,-1,39,15,50,houses
house_old_ranch_07,RESIDENTIALNEW,2,-1,52,16,42,houses
house_old_ranch_08,RESIDENTIALNEW,2,-4,52,21,52,houses
house_old_ranch_09,RESIDENTIALOLD,2,-4,44,16,38,houses
house_old_ranch_10,RESIDENTIALOLD,2,-5,40,19,44,houses
house_old_spanish_01,RESIDENTIALNEW,2,-15,31,25,49,houses
house_old_tudor_01,RESIDENTIALOLD,2,-30,56,66,45,houses
house_old_tudor_02,RESIDENTIALOLD,2,-1,32,21,46,houses
house_old_tudor_03,RESIDENTIALOLD,2,-6,34,25,53,houses
house_old_tudor_04,RESIDENTIALOLD,2,-4,31,26,46,houses
house_old_tudor_05,RESIDENTIALOLD,2,-4,29,26,44,houses
house_old_tudor_06,RESIDENTIALOLD,2,-4,33,23,53,houses
house_old_victorian_01,RESIDENTIALOLD,2,-10,49,27,56,houses
house_old_victorian_02,RESIDENTIALOLD,2,-4,33,29,53,houses
house_old_victorian_03,RESIDENTIALOLD,2,-5,38,42,53,houses
house_old_victorian_04,RESIDENTIALOLD,2,-5,38,31,52,houses
house_old_victorian_05,RESIDENTIALOLD,2,-4,34,26,49,houses
house_old_victorian_06,RESIDENTIALOLD,2,-4,27,24,53,houses
house_old_victorian_07,RESIDENTIALOLD,2,-6,35,25,53,houses
house_old_victorian_08,RESIDENTIALOLD,2,-4,24,21,46,houses
house_old_victorian_09,RESIDENTIALOLD,2,-1,41,27,53,houses
house_old_victorian_10,RESIDENTIALOLD,2,-9,33,29,46,houses
house_old_victorian_11,RESIDENTIALOLD,2,-4,29,24,53,houses
house_old_victorian_12,RESIDENTIALOLD,2,-4,28,23,46,houses
house_old_victorian_13,RESIDENTIALOLD,2,-4,31,26,46,houses
//hwy_overpass_sign_01,BIOMEONLY,2,0,
indian_burial_grounds_01,NOZONE,2,-2,27,8,27,alone;mountain
installation_red_mesa,INDUSTRIAL,2,-24,46,34,46,alone;industrial
installation_red_mesa_security,NAVONLY,0,0,9,17,10,alone;industrial
//installation_red_mesa_sign,NAVONLY,2,0,
//junkyard_01,NONE,0,-6,81,23,162,industrial;hillbillyjunk
junkyard_lg_01,INDUSTRIAL,0,-3,74,19,87,industrial;hillbillyjunk
junkyard_med_01,INDUSTRIAL,0,-3,61,18,52,industrial;hillbillyjunk
lodge_01,COMMERCIAL,2,-4,58,23,55,alone
mp_waste_bldg_01_red,DOWNTOWN;COMMERCIAL;INDUSTRIAL,1,0,31,17,27,citycenter
mp_waste_bldg_01_tan,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,31,17,26,citycenter
mp_waste_bldg_01_white,DOWNTOWN;COMMERCIAL;INDUSTRIAL,3,0,31,17,26,citycenter
mp_waste_bldg_02_red,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,31,17,39,citycenter
mp_waste_bldg_02_tan,DOWNTOWN;COMMERCIAL;INDUSTRIAL,1,0,31,17,39,citycenter
mp_waste_bldg_03_red,DOWNTOWN;COMMERCIAL;INDUSTRIAL,1,0,20,27,21,citycenter
mp_waste_bldg_03_tan,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,20,27,21,citycenter
mp_waste_bldg_03_white,DOWNTOWN;COMMERCIAL;INDUSTRIAL,3,0,20,27,21,citycenter
mp_waste_bldg_04_grey,DOWNTOWN;COMMERCIAL;INDUSTRIAL,3,0,14,17,38,citycenter
mp_waste_bldg_04_red,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,14,17,38,citycenter
mp_waste_bldg_04_tan,DOWNTOWN;COMMERCIAL;INDUSTRIAL,1,0,13,16,37,citycenter
mp_waste_bldg_04_white,DOWNTOWN;COMMERCIAL;INDUSTRIAL,0,0,14,17,38,citycenter
mp_waste_bldg_05_grey,DOWNTOWN;COMMERCIAL;INDUSTRIAL,3,0,20,13,22,citycenter
mp_waste_bldg_06_grey,DOWNTOWN;COMMERCIAL;INDUSTRIAL,3,0,23,22,34,citycenter
mp_waste_bldg_governer_08,DOWNTOWN,3,0,28,15,46,citycenter
//mp_waste_cover_01,NAVONLY,2,0,
//mp_waste_cover_02,NAVONLY,2,0,
//mp_waste_hole_asphalt_01,NAVONLY,2,0,
//mp_waste_hole_rock_01,NAVONLY,2,0,
//mp_waste_pile_01,BIOMEONLY,2,0,
//mp_waste_pile_02,BIOMEONLY,2,0,
mp_waste_pile_03,BIOMEONLY,0,0,4,3,4,houses
mp_waste_pile_04,BIOMEONLY,0,0,7,15,7,houses
mp_waste_pile_05,BIOMEONLY,0,0,9,4,9,houses
mp_waste_sewer_hole_1,COMMERCIAL,0,-10,33,32,31,houses
//mp_waste_sewer_hole_2,NAVONLY,0,0,25,11,23,houses
//mp_waste_tree_pile_01,NAVONLY,2,0,
//mp_waste_tree_pile_02,NAVONLY,2,0,
//navezgane_waterfall_01,NONE,2,0,
oldwest_business_01,COMMERCIAL,2,-5,17,20,27,oldwest
oldwest_business_02,COMMERCIAL,2,-1,23,15,26,oldwest
oldwest_business_03,COMMERCIAL,2,-1,50,15,20,oldwest
oldwest_business_04,COMMERCIAL,2,-1,21,14,19,oldwest
oldwest_business_05,COMMERCIAL,2,-1,19,9,14,oldwest
oldwest_business_06,COMMERCIAL,2,-1,30,17,24,oldwest
oldwest_business_07,COMMERCIAL,2,-1,13,10,17,oldwest
oldwest_business_08,COMMERCIAL,2,-3,28,19,26,oldwest
oldwest_business_09,COMMERCIAL,2,-1,13,11,16,oldwest
oldwest_business_10,COMMERCIAL,2,-5,20,22,26,oldwest
oldwest_business_11,COMMERCIAL,2,-1,19,17,26,oldwest
oldwest_business_12,COMMERCIAL,2,-1,17,15,17,oldwest
oldwest_business_13,COMMERCIAL,2,-5,18,21,22,oldwest
oldwest_business_14,COMMERCIAL,2,-1,20,15,23,oldwest
oldwest_church,RESIDENTIALOLD,2,-4,34,28,40,oldwest
oldwest_coal_factory,INDUSTRIAL,3,-13,33,31,39,oldwest
oldwest_gallows,COMMERCIAL,2,-1,13,8,16,farm;oldwest;hillbillyjunk
oldwest_jail,COMMERCIAL,2,-1,19,9,18,oldwest
oldwest_stables,INDUSTRIAL,2,-1,25,11,24,oldwest
oldwest_strip_01,COMMERCIAL,2,-5,52,21,23,oldwest
oldwest_strip_02,COMMERCIAL,2,-1,52,16,28,oldwest
oldwest_strip_03,COMMERCIAL,2,-1,52,20,27,oldwest
oldwest_strip_04,INDUSTRIAL,2,-1,37,19,28,oldwest
oldwest_watertower,INDUSTRIAL,2,-1,17,15,17,oldwest

parking_garage_01,COMMERCIAL;DOWNTOWN,2,0,43,18,44,citycenter;downtown
parking_lot_01,COMMERCIAL;DOWNTOWN,1,-1,35,11,35,downtown;smalltown
parking_lot_02,COMMERCIAL;DOWNTOWN,1,-1,35,11,35,downtown;smalltown;carlot
parking_lot_03,COMMERCIAL;DOWNTOWN,1,-1,35,11,35,downtown;smalltown;carlot
park_01,RESIDENTIALOLD;RESIDENTIALNEW,0,-1,53,10,53,smalltown;downtown
park_02,RESIDENTIALOLD;RESIDENTIALNEW,0,-1,28,5,46,smalltown;downtown

//perishton_city_blk_01,DEVONLY,2,0,
//perishton_city_blk_02,DEVONLY,2,0,
//perishton_city_blk_03,DEVONLY,2,0,
//perishton_city_blk_04,DEVONLY,2,0,
//perishton_city_sign_01,NAVONLY,2,0,
//perishton_median_01,NAVONLY,2,0,
player_start1,NAVONLY,0,0,6,2,6,alone;smalltown;downtown;hillbillyjunk
player_start2,NAVONLY,0,0,4,2,8,alone;smalltown;downtown;hillbillyjunk
player_start3,NAVONLY,0,0,5,2,6,alone;smalltown;downtown;hillbillyjunk
player_start4,NAVONLY,0,0,5,2,6,alone;smalltown;downtown;hillbillyjunk
player_start5,NAVONLY,0,0,6,2,8,alone;smalltown;downtown;hillbillyjunk
player_start6,NAVONLY,0,0,6,2,9,alone;smalltown;downtown;hillbillyjunk


police_station1,DOWNTOWN,1,-1,42,17,38,smalltown;downtown
pond01,ANY,2,-2,14,4,15,farm;alone;smalltown
pond02,ANY,2,-10,33,13,31,farm;alone;smalltown
post_office_med_01,DOWNTOWN,2,-4,39,21,41,smalltown;downtown
post_office_sm_01,DOWNTOWN,2,-1,22,11,34,smalltown;downtown
potatofield_sm,RESIDENTIALOLD;INDUSTRIAL,2,-1,12,2,12,farm
prison_01,DOWNTOWN,2,-1,38,19,48,industrial;smalltown
ranger_station1,COMMERCIAL,0,0,19,14,19,alone;mountain

remnant_burnt_01,RESIDENTIALOLD,2,-6,26,21,38,houses
remnant_burnt_02,RESIDENTIALOLD,2,-5,21,20,36,houses
remnant_burnt_03,RESIDENTIALOLD,2,-6,29,21,41,houses
remnant_burnt_04,COMMERCIAL,2,-1,18,7,33,houses
remnant_burnt_05,INDUSTRIAL,2,-2,31,40,28,smalltown;downtown
remnant_burnt_06,INDUSTRIAL,2,-1,42,15,43,houses
remnant_burnt_07,INDUSTRIAL,2,-8,22,15,36,smalltown;downtown
remnant_burnt_08,COMMERCIAL,2,-2,31,12,35,houses
remnant_burnt_09,RESIDENTIALOLD,2,-1,19,14,26,houses
remnant_burnt_deco_01,BIOMEONLY,0,0,8,10,8,houses
remnant_burnt_deco_02,BIOMEONLY,0,0,6,7,7,houses
remnant_house_01,RESIDENTIALOLD,2,-1,41,14,51,houses
remnant_house_02,RESIDENTIALOLD,2,-4,42,20,52,houses
remnant_house_03,RESIDENTIALOLD,2,-1,27,21,53,houses
remnant_house_04,RESIDENTIALOLD,2,-1,26,12,53,houses
remnant_house_05,RESIDENTIALOLD,2,-1,45,16,52,houses
remnant_house_06,RESIDENTIALOLD,2,-1,27,15,53,houses
remnant_house_07,RESIDENTIALOLD,2,-1,22,14,46,houses
remnant_house_08,RESIDENTIALOLD,2,-1,27,15,46,houses
remnant_house_09,RESIDENTIALOLD,2,-1,27,16,46,houses
remnant_house_10,RESIDENTIALOLD,2,-4,27,27,35,houses
remnant_mill_01,INDUSTRIAL,2,-1,41,26,50,oldwest
remnant_oldwest_01,INDUSTRIAL,2,-1,13,5,13,oldwest
remnant_oldwest_02,INDUSTRIAL,2,-1,10,4,10,oldwest
remnant_oldwest_03,INDUSTRIAL,2,-1,8,7,8,oldwest
remnant_oldwest_04,RESIDENTIALOLD,2,-1,16,13,27,oldwest
remnant_oldwest_05,RESIDENTIALOLD,2,-1,15,12,18,oldwest
remnant_oldwest_06,RESIDENTIALOLD,2,-1,17,14,17,oldwest
remnant_oldwest_07,COMMERCIAL,2,-1,19,16,26,oldwest
remnant_oldwest_08,COMMERCIAL,2,-5,20,22,24,oldwest
remnant_oldwest_09,INDUSTRIAL,2,-1,36,10,39,oldwest


//resource_coal_pile,BIOMEONLY,2,0,
//resource_lead_pile,BIOMEONLY,2,0,
//resource_nitrate_pile,BIOMEONLY,2,0,
//resource_oilshale_pile,BIOMEONLY,2,0,
//roadblock_01,NAVONLY,2,0,
//road_railing_long_filled_01,NAVONLY,2,0,
//rock_form01,BIOMEONLY,2,-1,
//rock_form02,BIOMEONLY,2,-2,
//rock_form03,NONE,2,-1,
//rock_form04,NONE,2,-1,
//rock_form05,NONE,2,-1,

//nice POI, but sawmill only works on SNOW!
//sawmill_01_snow,INDUSTRIAL,2,-1,75,28,100,alone

school_01,DOWNTOWN;RESIDENTIALNEW,2,-1,100,17,75,smalltown;downtown
school_daycare_01,DOWNTOWN;RESIDENTIALNEW,2,-1,27,13,51,smalltown;downtown
school_k6_01,DOWNTOWN;RESIDENTIALNEW,2,-1,49,17,79,smalltown;downtown
settlement_01,COMMERCIAL,2,-1,38,19,38,alone
settlement_trader_01,ANY,2,-1,37,24,37,trader
settlement_trader_02,ANY,2,-1,35,32,35,trader
settlement_trader_03,ANY,2,-1,43,24,41,trader
settlement_trader_04,ANY,2,-1,41,27,46,trader
settlement_trader_05,ANY,2,-8,45,28,49,trader
// sign_260_east_speed_65,NAVONLY,2,0,
// sign_260_west_speed_65,NAVONLY,2,0,
// sign_73_north,ANY,1,0,
// sign_73_south,ANY,1,0,
// sign_albuquerque,ANY,2,0,
// sign_ansel_adams_river,NAVONLY,2,0,
// sign_arrowhead_apache,NAVONLY,2,0,
// sign_camp_fish,NAVONLY,2,0,
// sign_caution,NAVONLY,2,0,
// sign_hazardous_waste,NAVONLY,2,0,
// sign_info_center1,NAVONLY,2,0,
// sign_navezgane_natl_forest,NAVONLY,2,0,
// sign_phoenix,ANY,2,0,
// sign_road_work,NAVONLY,2,0,
// sign_school_25,NAVONLY,2,0,
// sign_slow,ANY,2,0,
// sign_speed_25,ANY,2,0,
// sign_speed_35,ANY,2,0,
// sign_speed_45,ANY,2,0,
// sign_speed_55,NAVONLY,2,0,
// sign_spillway_lake,NAVONLY,2,0,
// sign_stop,NAVONLY,2,0,
// sign_stop_4way,NAVONLY,2,0,
skate_park_01,COMMERCIAL,2,-4,44,16,44,downtown
skyscraper_01,DOWNTOWN,2,-1,93,134,87,citycenter
skyscraper_02,DOWNTOWN,2,-1,47,66,76,citycenter
skyscraper_03,DOWNTOWN,2,-1,70,78,62,citycenter
skyscraper_04,DOWNTOWN,2,-1,54,59,40,citycenter
store_autoparts_01,COMMERCIAL,2,-5,56,18,46,smalltown;downtown
store_bank_lg_01,COMMERCIAL,2,-6,41,17,47,smalltown;downtown
store_book_lg_01,COMMERCIAL,2,-1,38,21,47,smalltown;downtown
store_book_sm_01,COMMERCIAL,2,-1,25,12,45,smalltown;downtown
store_grocery_lg_01,COMMERCIAL,2,-1,40,13,46,smalltown;downtown
store_grocery_sm_01,COMMERCIAL,2,-1,25,12,45,smalltown;downtown
store_gun_lg_01,COMMERCIAL,2,-1,45,13,45,smalltown;downtown
store_gun_sm_01,COMMERCIAL,2,-1,31,13,45,smalltown;downtown
store_hardware_lg_01,COMMERCIAL,2,-1,41,12,51,smalltown;downtown
store_hardware_sm_01,COMMERCIAL,2,-1,23,12,44,smalltown;downtown
store_laundry_01,COMMERCIAL;DOWNTOWN,2,-1,30,13,48,smalltown;downtown
store_pawn_01,COMMERCIAL;DOWNTOWN,2,-1,39,18,47,smalltown;downtown
store_pharmacy_sm_01,COMMERCIAL,2,-1,32,12,46,smalltown;downtown
store_salon,COMMERCIAL;DOWNTOWN,2,-1,25,10,41,smalltown;downtown
//streets_essag,NAVONLY,2,0,
// streets_tran_and_lang,NAVONLY,2,0,
// street_apache,NAVONLY,2,0,
// street_bell_courtland,NAVONLY,2,0,
// street_coronado,NAVONLY,2,0,
// street_courtland_apache,NAVONLY,2,0,
// street_courtland_coronado,NAVONLY,2,0,
// street_courtland_east_260,NAVONLY,2,0,
// street_davis,NAVONLY,2,0,
// street_huenink_courtland,NAVONLY,2,0,
// street_lake_bell,NAVONLY,2,0,
// street_light_01,ANY,1,0,
// street_light_02,ANY,1,0,
// street_maple,NAVONLY,2,0,
// street_maple_courtland,NAVONLY,2,0,
// street_private,NAVONLY,2,0,
// street_tran_courtland,NAVONLY,2,0,
survivor_site_01,NOZONE,2,-1,25,10,25,alone;mountain
survivor_site_02,NOZONE,2,-1,25,10,25,alone;mountain
survivor_site_03,NOZONE,2,-1,25,14,25,alone;mountain
survivor_site_04,NOZONE,2,-4,25,20,25,alone;mountain
survivor_site_05,NOZONE,2,-1,25,17,25,alone;mountain
survivor_site_06,NOZONE,2,-4,25,20,25,alone;mountain
survivor_site_07,NOZONE,2,-1,25,16,25,alone;mountain
survivor_site_08,NOZONE,2,-1,25,23,25,alone;mountain
theater_01,COMMERCIAL;DOWNTOWN,2,0,39,23,44,downtown
theatre_01,COMMERCIAL;DOWNTOWN,2,-6,44,53,77,downtown
trailer_01,RESIDENTIALOLD,2,-1,25,7,25,alone;houses;hillbillytrailer
trailer_02,RESIDENTIALOLD,2,-1,32,8,32,alone;houses;hillbillytrailer
trailer_03,RESIDENTIALOLD,2,-1,25,9,25,alone;houses;hillbillytrailer
trailer_04,RESIDENTIALOLD,2,-1,25,9,25,alone;houses;hillbillytrailer
trailer_park_01,RESIDENTIALOLD,2,-1,44,11,44,houses;hillbillytrailer
// tree_bruntpine_cluster_01,NAVONLY,2,0,
// tree_bruntpine_cluster_02,NAVONLY,2,0,
// tree_burntpine_01,ANY,2,0,
// tree_burntpine_02,ANY,2,0,
// tree_burntpine_03,ANY,2,0,
// tree_cactus_01,NAVONLY,2,0,
// tree_cactus_02,NAVONLY,2,0,
// tree_cactus_03,NAVONLY,2,0,
utility_celltower_01,INDUSTRIAL,2,-1,28,40,24,alone;industrial
utility_celltower_02,INDUSTRIAL,2,-1,37,45,28,alone;industrial
utility_electric_co_01,INDUSTRIAL,2,-8,45,31,45,industrial
utility_refinery_01,INDUSTRIAL,2,-1,58,37,48,industrial
utility_waterworks_01,INDUSTRIAL,2,-14,45,31,45,industrial
vacant_lot_01,ANY,0,-1,23,13,30,downtown;hillbillyjunk
vacant_lot_02,ANY,0,-1,23,13,30,downtown;hillbillyjunk
vacant_lot_03,ANY,0,-1,16,4,30,downtown;hillbillyjunk
vacant_lot_04,ANY,2,-1,16,4,30,downtown;hillbillyjunk
vacant_lot_05,ANY,2,-1,27,4,41,downtown;hillbillyjunk
warehouse_01,NONE,0,-1,29,12,55,industrial
waste_rubble_bldg_01,DOWNTOWN;COMMERCIAL;INDUSTRIAL,0,0,6,11,6,industrial
waste_rubble_bldg_02,DOWNTOWN;COMMERCIAL;INDUSTRIAL,1,0,9,10,10,industrial
waste_rubble_bldg_03,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,7,9,9,industrial
waste_rubble_bldg_04,DOWNTOWN;COMMERCIAL;INDUSTRIAL,0,0,16,21,14,industrial
waste_rubble_bldg_05,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,36,10,35,industrial
waste_rubble_bldg_06,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,36,14,35,industrial
waste_rubble_bldg_07,DOWNTOWN;COMMERCIAL;INDUSTRIAL,3,0,16,19,14,industrial
waste_rubble_bldg_08,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,34,9,33,industrial
waste_rubble_bldg_09,DOWNTOWN;COMMERCIAL;INDUSTRIAL,2,0,11,20,12,industrial
//waste_rubble_highway_01,NAVONLY,2,0,
water_tower_01,INDUSTRIAL,2,-1,15,17,15,smalltown;hillbillyjunk
water_tower_02,INDUSTRIAL,2,-1,15,17,17,smalltown;hillbillyjunk
water_tower_03,RESIDENTIALOLD;RESIDENTIALNEW;INDUSTRIAL,2,-1,21,21,24,farm;smalltown

scav_CanucksCoffee(NAGG),Commercial;downtown,0,-2,42,11,50,citycenter;downtown;smalltown 
scav_HNHouse(Hernan),ResidentialNew,2,-5,49,23,55,houses 
scav_Lighthouse(NAGG),ResidentialOld,1,-8,46,48,46,citycenter;downtown;smalltown;alone 
scav_Vet_Clinic(Firecat),downtown;Commercial,0,-2,64,17,73,smalltown;alone;downtown 
scav_NoHopeMall(Hernan),Commercial;downtown,2,-7,205,50,149,downtown;smalltown;unique
scav_Bus_Depot(Hydro),Commercial;Downtown,2,-1,61,8,29,citycenter;smalltown;downtown;alone 
scav_Graveyard(Hydro),BiomeOnly;Downtown,2,-4,45,23,46,smalltown;downtown;alone 
scav_HellMart(RihasaurusRex),Commercial;downtown,2,-2,75,24,59,smalltown;downtown;unique 

scav_Hicks_Family_Diner(Hydro),Commercial;Downtown;Industrial,2,-1,33,11,44,citycenter;smalltown;downtown 
scav_MediumHouse(Unknown),ResidentialNew,1,-1,17,17,20,houses 
scav_Oil_PumpJack(Hydro),Industrial,2,-1,20,7,14,smalltown;alone 
scav_SlavCo_SuperMarket(Hydro),Commercial;Downtown,2,-1,88,12,98,citycenter;downtown 
scav_Small_House(TheTruJames),ResidentialNew;ResidentialOld;Residential,2,-2,31,8,24,houses
scav_StripMall(Axebeard),Commercial;Downtown,2,-1,104,22,57,citycenter;downtown 
scav_Strip_Mall_Large(Hydro),Commercial;Downtown,2,-1,61,11,53,citycenter;downtown 
scav_The_Home(Riahbug08),ResidentialNew;ResidentialOld;Residential,2,-11,37,51,41,houses
scav_Wank_Bank(Hydro),Commercial;Downtown,2,-9,29,15,40,citycenter;downtown 

scav_7EndStore(Jackelmyer),Commercial;downtown,0,-1,29,11,46,citycenter;downtown;smalltown 
scav_BlueHouse(TopMinder_Pille),ResidentialOld,2,-2,53,22,42,houses
scav_BoxingGym(KamR),Commercial;downtown,2,-1,49,14,51,citycenter;downtown;smalltown 
scav_Car_Dealership_new(p1ut0nium),Commercial,2,-5,28,16,41,citycenter;smalltown;downtown;carlot 
scav_CrashedAirplane(LazMan),NoZone,2,-6,48,18,78,smalltown;alone;unique 
scav_CrashedAirplane(LazMan),NoZone,2,-6,48,18,78,smalltown;alone;unique 


scav_deaddog_saloon(Libby),Commercial,2,0,46,20,24,smalltown;unique 
scav_DriveIn(Rick),Commercial,2,-4,57,20,90,smalltown;alone;unique 


scav_house01(Guppycur),ResidentialOld,0,-4,36,17,25,houses 
scav_house02(Guppycur),ResidentialOld,0,-5,30,19,23,houses
scav_house03(Guppycur),ResidentialOld,0,-4,21,22,22,houses
scav_house04(Guppycur),ResidentialOld,0,-2,18,17,17,houses
scav_house05(Guppycur),ResidentialOld,0,-2,19,17,19,houses
scav_LittleLeagueBaseballShelter(Curmudgeon),ResidentialNew;ResidentialOld;Residential,2,-5,59,17,59,smalltown;downtown;unique 

scav_Firehouse(Warsaken),ResidentialNew,2,-5,44,18,44,citycenter;downtown 
scav_FuneralHome(Wsiegel),Commercial;ResidentialNew,2,-6,30,25,32,citycenter;downtown;smalltown 
scav_FuneralParlor(Zombieman),Commercial;ResidentialNew;ResidentialOld,2,-10,48,21,50,smalltown;downtown;alone 
scav_Motel(Limodor),Commercial;ResidentialOld,2,-10,70,15,32,smalltown;alone 
scav_NotreDamn(tehkarma),Downtown,1,-9,105,51,39,citycenter;downtown;unique 
scav_NotreDamn(tehkarma),Downtown,1,-9,105,51,39,citycenter;downtown;unique 

scav_PetStore(Jackelmyer),Commercial,0,-5,72,16,76,citycenter;smalltown;downtown 
scav_rave_factory(orak),Industrial,2,-5,112,46,99,alone;unique  


scav_RedCatDiner(Najax),Commercial,2,-1,40,10,41,citycenter;smalltown;downtown 
scav_SnackShack(bigstep70),Commercial;Downtown;ResidentialNew;ResidentialOld,2,-1,41,9,31,citycenter;smalltown;downtown 
scav_Subday(Swolk),Commercial;Downtown;ResidentialNew,2,-2,26,13,29,citycenter;smalltown;downtown 
scav_TacoBele(Swolk),Commercial;Downtown;ResidentialNew,2,-6,56,17,64,citycenter;smalltown;downtown 
scav_Walcart(Swolk),Commercial;Downtown,2,-6,64,20,98,smalltown;downtown;alone;unique  


scav_ZHigh(Volar),ResidentialNew;ResidentialOld,0,-10,89,26,93,citycenter;smalltown;downtown;alone 


scav_InsaneAsylum(NAGG),Downtown;ResidentialOld,0,-9,130,48,77,citycenter;unique 
scav_IZEA(NAGG),Commercial;Downtown,2,-6,152,26,117,citycenter;downtown;unique
scav_Library(NAGG),Commercial;Downtown,2,-8,19,25,38,citycenter;downtown;smalltown;unique
scav_InsaneAsylum(NAGG),Downtown;ResidentialOld,0,-9,130,48,77,citycenter;unique 
scav_IZEA(NAGG),Commercial;Downtown,2,-6,152,26,117,citycenter;downtown;unique
scav_Library(NAGG),Commercial;Downtown,2,-8,19,25,38,citycenter;downtown;smalltown;unique


scav_QuietPines(Hernan),ResidentialNew,2,-6,105,25,122,alone;unique


scav_RestaurantItalo(Axebeard),Downtown;Commercial,2,-1,55,12,67,citycenter;downtown 
scav_TrailerMix_01(Vanilla),ResidentialOld;Residential,1,-5,36,14,36,smalltown;downtown;alone 
scav_TrailerMix_02(Vanilla),ResidentialOld;Residential,2,-5,36,13,35,smalltown;downtown;alone 
scav_TrailerMix_03(SandyBeaches),ResidentialOld;Residential,2,-5,36,13,36,smalltown;downtown;alone 
scav_TrailerMix_04(SandyBeaches),ResidentialOld;Residential,2,-5,40,22,39,smalltown;downtown;alone 
scav_TrailerMix_05(sinder),ResidentialOld;Residential,2,-5,36,14,41,smalltown;downtown;alone 
scav_TrailerMix_06(knightrath),ResidentialOld;Residential,2,-5,36,14,40,smalltown;downtown;alone 
scav_Vista(Makabriel),Downtown;ResidentialNew,0,-12,157,53,149,citycenter;smalltown;downtown;unique
scav_WalkingDeadPrison(Laz_Man),Industrial,2,-13,212,55,200,smalltown;alone;unique

scav_Vista(Makabriel),Downtown;ResidentialNew,0,-12,157,53,149,citycenter;smalltown;downtown;unique


scav_TrailerMix_01(Vanilla),ResidentialOld;Residential,1,-5,36,14,36,alone;houses;hillbillytrailer
scav_TrailerMix_02(Vanilla),ResidentialOld;Residential,2,-5,36,13,35,alone;houses;hillbillytrailer
scav_TrailerMix_03(SandyBeaches),ResidentialOld;Residential,2,-5,36,13,36,alone;houses;hillbillytrailer
scav_TrailerMix_04(SandyBeaches),ResidentialOld;Residential,2,-5,40,22,39,alone;houses;hillbillytrailer
scav_TrailerMix_05(sinder),ResidentialOld;Residential,2,-5,36,14,41,alone;houses;hillbillytrailer
scav_TrailerMix_06(knightrath),ResidentialOld;Residential,2,-5,36,14,40,alone;houses;hillbillytrailer

scav_Book_Stop(Hydro),Commercial;Downtown,2,-1,25,8,35,citycenter;downtown 

scav_Junkyard(Hydro),Industrial,2,-1,23,5,23,industrial

scav_z09_Industrie_01(Zyncosa),Commercial;Industrial,0,-12,65,52,72,citycenter;industrial 
scav_z10_markt_02_sham(Zyncosa),Commercial;downtown,2,-2,17,29,21,smalltown;alone 
scav_z11_markt_03_pills(Zyncosa),Commercial;downtown,2,-2,17,29,21,smalltown;alone 
scav_z12_markt_04_guns(Zyncosa),Commercial;downtown,0,-2,17,29,21,smalltown;alone 
scav_z13_markt_05_books(Zyncosa),Commercial;downtown,0,-2,17,29,21,smalltown;alone 
scav_z14_markt_06_tools(Zyncosa),Commercial;downtown,0,-2,17,29,21,smalltown;alone 
scav_z15_HausApts_04(Zyncosa),ResidentialOld; Residential,2,-2,48,42,50,citycenter;smalltown;downtown 
scav_z17_Haus_05(Zyncosa),ResidentialOld; Residential,2,-7,36,47,33,houses
scav_z18_Haus_06(Zyncosa),ResidentialOld; Residential,2,-12,25,39,26,houses
scav_z19_Industrie_02_auto(Zyncosa),Industrial,2,-15,39,34,49,industrial;citycenter 

scav_sawmill(Pille),Commercial,1,-6,50,28,51,downtown;smalltown;alone 
scav_z22_Haus_07(Zyncosa),Commercial;downtown,2,-9,46,37,51,citycenter;downtown 
scav_z25_Industrie_06(Zyncosa),Commercial;Downtown;Industrial,2,-15,28,55,31,downtown;smalltown;alone;citycenter 
scav_z26_Haus_08(Zyncosa),Downtown;ResidentialNew;ResidentialOld,2,-7,17,48,17,downtown;smalltown;houses 

scav_a16_abandoned_house_01(restoBy_sinder),ResidentialOld,2,-2,28,18,30,smalltown;houses 
scav_a16_abandoned_house_02(restoBy_sinder),ResidentialOld,2,-2,32,15,30,smalltown;houses 
scav_a16_abandoned_house_03(restoBy_sinder),ResidentialOld,2,-2,25,15,26,smalltown;houses 
scav_a16_abandoned_house_04(restoBy_sinder),ResidentialOld,2,-2,36,15,34,smalltown;houses 
scav_a16_abandoned_house_05(restoBy_sinder),ResidentialOld,2,-2,33,18,35,smalltown;houses 
scav_a16_abandoned_house_06(restoBy_sinder),ResidentialOld,2,-2,35,16,36,smalltown;houses 
scav_a16_abandoned_house_07(restoBy_sinder),ResidentialOld,2,-2,41,17,35,smalltown;houses 
scav_a16_abandoned_house_08(restoBy_sinder),ResidentialOld,2,-6,37,22,37,smalltown;houses 
scav_a16_skyscraper_02(restoBy_Robear),Downtown,2,-4,55,64,83,citycenter 
scav_a16_skyscraper_03(restoBy_Robear),Downtown,2,-2,76,79,68,citycenter 
scav_a16_skyscraper_04(restoBy_Robear),Downtown,2,-3,54,55,40,citycenter 

scav_a16_abandoned_house_01(restoBy_sinder),ResidentialOld,2,-2,28,18,30,smalltown;houses  
scav_a16_abandoned_house_02(restoBy_sinder),ResidentialOld,2,-2,32,15,30,smalltown;houses  
scav_a16_abandoned_house_03(restoBy_sinder),ResidentialOld,2,-2,25,15,26,smalltown;houses  
scav_a16_abandoned_house_04(restoBy_sinder),ResidentialOld,2,-2,36,15,34,smalltown;houses  
scav_a16_abandoned_house_05(restoBy_sinder),ResidentialOld,2,-2,33,18,35,smalltown;houses  
scav_a16_abandoned_house_06(restoBy_sinder),ResidentialOld,2,-2,35,16,36,smalltown;houses  
scav_a16_abandoned_house_07(restoBy_sinder),ResidentialOld,2,-2,41,17,35,smalltown;houses  
scav_a16_abandoned_house_08(restoBy_sinder),ResidentialOld,2,-6,37,22,37,smalltown;houses  
